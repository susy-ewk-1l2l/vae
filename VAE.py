import uproot
import os
os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path
import datetime
    
print('Using tf version {}'.format(tf.__version__))

path = '/eos/atlas/atlascerngroupdisk/phys-hmbs/susy-ewk/EWK1L2L_ANA-SUSY-2024-05/1L2L-samples/1L2Lv5/2022/'

features = ['met', 'mt', 'lep1Pt', 'jet1Pt', 'jet2Pt', 'dPhi_lep_met'] # input features 

preselection = "RunNumber>0"

training_events = 100000 # How many events to keep during training. -1 to train over all events, otherwise a value. Make sure you have events from all samples.

store_encodeddata = True
'''
# Define latent_dim and other layers as you already have
if len(features) % 2 == 0:
    latent_dim = int(len(features) / 4)
else:
    latent_dim = int((len(features)-1) / 4)
'''
latent_dim = 4

def read_training_samples(sample):

    print("Reading training samples")
    ####################################################
    root_file, sample_name = sample.split(":", 1)
    print('Using samples in {}{}'.format(path, root_file))  
    data_file = uproot.open(path+sample)
    branches = list(data_file.keys())
    df_data = data_file.arrays(branches,preselection,library="pd")
    df_data['Sample'] = sample_name
    df_data = df_data[:training_events]
    print(df_data)
    return(df_data)

def read_testing_samples(sample):

    print("Reading testing samples")
    ####################################################
    root_file, sample_name = sample.split(":", 1)
    print('Using samples in {}{}'.format(path, root_file))    
    df_file = uproot.open(path+sample)
    branches = list(df_file.keys())
    df = df_file.arrays(branches,preselection,library="pd")
    df['Sample'] = sample_name
    print(df)
    return(df)

def sampling(mean, log_var, latent_dim):
    epsilon = tf.random.normal(shape=(tf.shape(mean)[0], latent_dim))
    return mean + tf.exp(0.5 * log_var) * epsilon
        
def train(df):

    print("Training")
    
    encoder = tf.keras.Sequential([
        tf.keras.layers.Input(shape=(len(features),)),
        tf.keras.layers.Dense(256, activation='relu'),
        tf.keras.layers.Dense(128, activation='relu', kernel_initializer='he_normal'),
        tf.keras.layers.Dense(latent_dim + latent_dim)  # Two outputs for mean and variance
    ])

    decoder = tf.keras.Sequential([
        tf.keras.layers.Input(shape=(latent_dim,)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(256, activation='relu'),
        tf.keras.layers.Dense(len(features))  # Output layer to reconstruct input
    ])

    inputs = tf.keras.layers.Input(shape=(len(features),))
    mean_log_var = encoder(inputs)
    mean, log_var = tf.split(mean_log_var, num_or_size_splits=2, axis=-1)
    z = sampling(mean, log_var, latent_dim)
    outputs = decoder(z)
    vae = tf.keras.Model(inputs, outputs)

    kl_loss = -0.5 * tf.reduce_sum(1 + log_var - tf.square(mean) - tf.exp(log_var), axis=-1)
    reconstruction_loss = tf.reduce_sum(tf.keras.losses.mean_squared_error(inputs, outputs), axis=-1)
    vae_loss = tf.reduce_mean(reconstruction_loss + kl_loss)
    vae.add_loss(vae_loss)
    
    from sklearn.preprocessing import StandardScaler    
    scaler = StandardScaler()
    df[features] = scaler.fit_transform(df[features])

    vae.compile(optimizer='adam')
    vae.fit(np.asarray(df[features]), np.asarray(df[features]), epochs=5)
    
    encoder.save('encoder.keras')
    decoder.save('decoder.keras')    
    vae.save('vae.keras')

    return encoder, decoder, vae
  
        
def test(df, encoder, decoder):

    # Use the encoder to get the latent representations
    encoded_data = encoder.predict(np.asarray(df[features]))
    encoder.summary()
    print(f'encoded_data = {encoded_data}, shape = {encoded_data.shape}')

    # Split into mean and log variance
    mean_data, log_var_data = tf.split(encoded_data, num_or_size_splits=2, axis=-1)

    # Sample z using the sampling function
    z_data = sampling(mean_data, log_var_data, latent_dim)

    decoder.summary()
    # Use the decoder to get the reconstructed data
    predictions = decoder.predict(z_data)
    print(f'predictions = {predictions}, shape = {predictions.shape}')

    # Calculate KL loss
    kl_loss = -0.5 * tf.keras.backend.sum(1 + encoded_data[:, latent_dim:] - tf.keras.backend.square(encoded_data[:, :latent_dim]) -tf.keras.backend.exp(encoded_data[:, latent_dim:]), axis=-1)
    print(f'Kullback-Leibler Divergence function = {kl_loss}')

    # Calculate reconstruction loss
    reconstruction_loss = tf.keras.losses.mean_squared_error(np.asarray(df[features]), predictions)
    print(f'p(x|z) = {reconstruction_loss}')

    # VAE loss
    vae_loss = reconstruction_loss + kl_loss
    print(f'VAE loss p(x|z) + KL = {vae_loss}')

    decoded_dataset = pd.DataFrame(predictions, columns=features, index=df.index)
    decoded_dataset['MSE_Loss'] = reconstruction_loss
    decoded_dataset['KL_Loss'] = vae_loss
    decoded_dataset['Total_Loss'] = vae_loss
    return decoded_dataset

def save_trees(original_dataset, decoded_dataset, sample):

    outputdir_name = 'output_withVAE'
    if not os.path.exists(outputdir_name):
        os.system('mkdir {}'.format(outputdir_name))
        print(f"Directory '{outputdir_name}' created.")
    else:
        print(f"Directory '{outputdir_name}' already exists.")

    decoded_dataset.columns = [f"VAE_{col}" for col in decoded_dataset.columns]
    merged_dataset = pd.concat([original_dataset, decoded_dataset], axis=1)

    root_file, sample_name = sample.split(":", 1)
    branches = list(merged_dataset.keys())
    branches.remove('Sample') # Sample branch should not be added on output trees
    print("Processing: {} sample".format(sample_name))
    print("Dataframe contaning: {} branches".format(branches))
    
    distributions_dict={}
    for branch in branches:
        distributions_dict[branch] = np.array(merged_dataset[branch])
            
    with uproot.recreate("{}/{}".format(outputdir_name,root_file)) as f:
        f[sample_name] = distributions_dict

def main():

    df_training = read_training_samples('data_merged_processed.root:data22')
    encoder, decoder, vae = train(df_training)

    # Data original + VAE 
    df = read_testing_samples('data_merged_processed.root:data22')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'data_merged_processed.root:data22')
        
    # W+jets + VAE
    df = read_testing_samples('wjets_merged_processed.root:wjets_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'wjets_merged_processed.root:wjets_NoSys')
    
    # ttbar + VAE
    df = read_testing_samples('ttbar_merged_processed.root:ttbar_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'ttbar_merged_processed.root:ttbar_NoSys')
    
    # Z+jets + VAE
    df = read_testing_samples('zjets_merged_processed.root:zjets_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'zjets_merged_processed.root:zjets_NoSys')
  
    # singletop + VAE
    df = read_testing_samples('singletop_merged_processed.root:singletop_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'singletop_merged_processed.root:singletop_NoSys')
    
    # triboson + VAE
    df = read_testing_samples('triboson_merged_processed.root:triboson_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'triboson_merged_processed.root:triboson_NoSys')
    
    # gammajet + VAE
    df = read_testing_samples('gammajet_merged_processed.root:gammajet_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'gammajet_merged_processed.root:gammajet_NoSys')
     
    # signal_200_50 + VAE
    df = read_testing_samples('signal_200_50_merged_processed.root:signal_200_50_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'signal_200_50_merged_processed.root:signal_200_50_NoSys')
    
    # signal_800_0 + VAE
    df = read_testing_samples('signal_800_0_merged_processed.root:signal_800_0_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'signal_800_0_merged_processed.root:signal_800_0_NoSys')
    
if __name__ == "__main__":
    main()
